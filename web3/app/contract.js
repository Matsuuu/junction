var Web3 = require("web3");
var ObjectBuilder = require("./objectBuilder");

const web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));

var myAddress = "0x62b905e8c77e569dc2dd0f3c8a8729a1deb5635a";

let contractAddress = "0x802171c273872fe6d3268ad849b51000e4b89f6c";

let contractABI = [
	{
		"constant": false,
		"inputs": [],
		"name": "decrementCounter",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [],
		"name": "incrementCounter",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "getCount",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	}
];


var contract = new web3.eth.Contract(contractABI, contractAddress);


contract.methods.incrementCounter();

let count = contract.methods.getCount();

let getOffers = () => {
	const dummyData = ObjectBuilder.getDummyData();
	let dataObjects = {"offers":  ObjectBuilder.mapOffers(dummyData)};
	return dataObjects;
}

let submitOffer = () => {
	let contractData = {
		recipient: "bc1qar0srrr7xfkvy5l643lydnw9re59gtzzwf5mdq",
		coordinates: "60.190474&24.8187844",
		offerPerUnit: 12,
		measurement: 2.7
	};
	//contract.methods.submitOffer(contractData.recipient, contractData.coordinates, contractData.offerPerUnit, contractData.measurement);
}

let addToFund = () => {

}


module.exports = {
	getCount: async function() {
		return await contract.methods.getCount().call();
	},
	addCount: function() {
		return contract.methods.incrementCounter().send({
			from: myAddress
		});
	},
	getOffers: function() {
		return getOffers();
	},
	submitOffer: function(req) {
		submitOffer();
		return "";
	}
}