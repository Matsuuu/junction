pragma solidity ^0.4.24;

contract Counter {
    uint private count = 0;

    function incrementCounter() public {
        count += 1;
    }
    function decrementCounter() public {
        count -= 1;
    }
    function getCount() public view returns (uint) {
        return count;
    }
}
